/*
	版权所有 2009-2023 荆门泽优软件有限公司
	保留所有权利
	官方网站：http://www.ncmem.com
	产品首页：http://www.ncmem.com/webapp/scp2/index.aspx
	控件下载：http://www.ncmem.com/webapp/scp2/pack.aspx
	示例下载：http://www.ncmem.com/webapp/scp2/versions.aspx
	联系邮箱：1085617561@qq.com
	联系QQ：1085617561
	版本：2.1.5
*/
function zyCaptureManager()
{
    //url=>res/
    //http://localhost:8888/filemgr/res/up6/up6.js=>
    this.getJsDir = function () {
        var js = document.scripts;
        var jsPath;
        for (var i = 0; i < js.length; i++) {
            if (js[i].src.indexOf("zyCapture/") > -1) {
                jsPath = js[i].src.substring(0, js[i].src.indexOf("zyCapture/"));
            }
        }
        return jsPath;
    };
    var rootDir = this.getJsDir() + "zyCapture/";
    //http://localhost/res/down2/
    var pathRes = rootDir + "";

	var _this = this;
	this.scpFF = null;
    this.scpIE = null;
    this.ui = { render:null,panel: null, ico: null, img: null, msg: null, per: null ,setup:null,dialog:0};
    this.event = {
        postComplete: function (e,src) {
            var img = '<img src="' + src + "?t=" + new Date().getTime() + '"/>';
            e.data.editor.insertContent(img);
        },
        hotKey: function (e) {
            setTimeout(function () {
                e.Capture2();
            }, 100);
        },
        webSocketClose:function(){
            _this.data.inited = false;
        },
        scriptReady: function () {
            $(function () {
                //加载
                if (_this.ui.render == null) {
                    _this.loadAuto();
                }
                else if (typeof (_this.ui.render) == "string") {
                    _this.load_to($("#" + _this.ui.render));
                }
                else if (typeof (_this.ui.render) == "object") {
                    _this.load_to(_this.ui.render);
                }
            });
        }
    };
    this.data={
        browser:{name:navigator.userAgent.toLowerCase(),ie:true,ie64:false,chrome:false,firefox:false,edge:false,arm64:false,mips64:false,platform:window.navigator.platform.toLowerCase()},
        editor:null,opened:false,parter:null,socket:null,tryConnect:true,inited:false,
        language:{
            en:{
                "CapForm": "Capture Form Selecter"
              , "CapFormTitle": "Choose Capture Form"
              , "CapFormTip": "Please set the window to the front which you want to intercept resize"
              , "BtnOk": "Ok"
              , "BtnCancel": "Cancel"
              , "RectSuze": "Rect Size"
              , "CurRGB": "Current RGB"
              , "QuckCap": "Double-click can be quickly completed Screenshot"
          },
            zh_cn:{
                "CapForm": "截屏选择窗口"
              , "CapFormTitle": "选择截屏窗口"
              , "CapFormTip": "请将您想要截取的窗口调整到最前"
              , "BtnOk": "确定"
              , "BtnCancel": "取消"
              , "RectSuze": "区域大小"
              , "CurRGB": "当前RGB"
              , "QuckCap": "双击可以快速完成截图"
          },
            zh_tw:{
                "CapForm": "截屏選擇視窗"
              , "CapFormTitle": "選擇截屏視窗"
              , "CapFormTip": "請將您想要截取的視窗調整到最前"
              , "BtnOk": "確定"
              , "BtnCancel": "取消"
              , "RectSuze": "區域大小"
              , "CurRGB": "當前RGB"
              , "QuckCap": "雙擊可以快速完成截圖"
          }
        },
        error:{
            "0": "就绪",
            "1": "发送数据错误",
            "2": "接收数据错误",
            "3": "域名未授权或为空",
            "4": "公司未授权或为空",
            "5": "nat app error"
        },
        state:{
            Ready : 0,
            Posting : 1,
            Stop : 2,
            Error : 3,
            GetNewID : 4,
            Complete : 5,
            WaitContinueUpload	: 6,
            None : 7,
            Waiting : 8
        },
        scripts: [
            "z.css"
        ],
        jsCount: 0//已经加载的脚本总数
    };
    this.api={
        run:function(){
            if (typeof navigator.msLaunchUri != 'undefined')
            {
                console.log(_this.Config.edge.protocol + "://" + _this.Config.edge.port);
                //up6://9006
                navigator.msLaunchUri(_this.Config.edge.protocol+"://"+_this.Config.edge.port, function ()
                {
                    console.log('应用打开成功');
                }, function ()
                {
                    console.log('启动失败');
                });
            }
        },
        runChr:function(){
            var protocol = _this.Config.edge.protocol + "://" + _this.Config.edge.port;
            var html = "<iframe id='scp-uri-fra' width=1 height=1 src='" + protocol + "'></iframe>";
            $("#scp-uri-fra").remove();
            $(document.body).append(html);
        },
        connect:function(){
            if (!_this.data.tryConnect) return;
            var con = new WebSocket('ws://127.0.0.1:' + _this.Config.edge.port);
            console.log("开始连接服务:" + 'ws://127.0.0.1:' + _this.Config.edge.port);
    
            // 打开Socket 
            con.onopen = function (event)
            {
                _this.data.socket = con;
                _this.data.tryConnect = false;
                console.log("服务连接成功");
    
                // 监听消息
                con.onmessage = function (event)
                {
                    _this.recvMessage(event.data);
                };
    
                // 监听Socket的关闭
                con.onclose = function (event)
                {
                    console.log("连接关闭");
                    _this.data.tryConnect=true;
                    _this.event.webSocketClose();//
                };
            };
            con.onerror = function (event)
            {
                _this.api.run();
                console.log("连接失败");
            };
        },
        close:function(){
            if (_this.data.socket) { _this.data.socket.close(1000,"close");}
        },
        send:function(p){
            if(_this.data.socket)_this.data.socket.send(JSON.stringify(p));
        },
        init:function(){
            var param = { name: "init", config: _this.Config,fields:_this.Config.Fields };
            _this.api.postMessage(param);
        },
        capture: function (opt) {
            var param = $.extend({}, opt,{ name: "capture" });
            _this.api.postMessage(param);
        },
        captureScreen: function (opt) {
            var param = $.extend({},opt,{ name: "capture_screen" });
            _this.api.postMessage(param);
        },
        captureRect: function (left, top, width, height) {
            var param = { name: "capture_rect" ,x: left, y: top, w: width, h: height };
            _this.api.postMessage(param);
        },
        paste: function () {
            var param = { name: "paste" };
            _this.api.postMessage(param);
        },
        postMessage:function(json){
            _this.data.parter.postMessage(JSON.stringify(json));
        },
        postMessage2:function(json){
            _this.api.send(json);
        },
        loadCss:function(url,callback){
            var css = document.createElement("link");
            css.setAttribute("rel", "stylesheet");
            css.setAttribute("type", "text/css");
            css.setAttribute("href", url);
            if(callback){
                css.onreadystatechange = function () {
                    if (css.readyState === "loaded" || css.readyState === "complete") {
                        // no need to be notified again
                        css.onreadystatechange = null;
                        // notify user
                        callback();
                    }
                };
    
                // other browsers
                css.onload = function () {
                    callback();
                };
            }
            css.onerror=function(){
                console.log("加载CSS错误，"+url);
            }
            document.head.appendChild(css);
        },
        loadJs:function(js,callback){
            var script = document.createElement("script");
            script.src = js;
    
            // monitor script loading
            // IE < 7, does not support onload
            if (callback) {
                script.onreadystatechange = function () {
                    if (script.readyState === "loaded" || script.readyState === "complete") {
                        // no need to be notified again
                        script.onreadystatechange = null;
                        // notify user
                        callback();
                    }
                };
    
                // other browsers
                script.onload = function () {
                    callback();
                };
            }
    
            // append and execute script
            document.head.appendChild(script);
        }
    };

	//全局配置信息
	this.Config = {
		  "PostUrl"		: "http://www.ncmem.com/upload.aspx"
		, "EncodeType"	: "utf-8"
		, "Version"		: "1,4,74,5817"
		, "Company"		: "荆门泽优软件有限公司"
		, "License2"	: ""
        , "Debug"       : false//是否打开调试模式
        , "LogFile"     : "F:\\log.txt"//日志文件路径
        , "FileFieldName": "img"//自定义图片文件字段名称。
        , "ImageMatch"  : ""//服务器返回数据匹配模式，正则表达式，提取括号中的地址
        , "ImageUrl"    : ""//自定义图片地址，格式"{url}"，{url}为固定变量，在此变量前后拼接图片路径，此变量的值为posturl返回的图片地址
		, "LanCur"	    : this.data.language.zh_cn//语言设置
		, "Quality"     : 100//jpg图片质量，仅对jpg格式有效
		, "ShowForm"	: true//是否显示截屏提示窗口
		, "ImageType"	: "png"//图片上传格式。png,jpg,bmp
		, "NameCrypto"	: "crc"//名称生成算法。crc,md5,sha1,uuid
		, "IcoPath"		: pathRes + "upload.gif"
        , "Cookie"      : ""
        , "HotKey"      : "Ctrl+Alt+Q"
        , "Authenticate": { "type": "ntlm", "name": "", "pass": "" }//域环境信息
        , UI: {
            dialog:{opacity:30/*选择窗口透明度,0:完全透明,255:完全不透明*/},
            selector: {
                border: "#DC143C"//选框边框
            },
            font: ["21", "22", "23", "24"],
            //文字特效：http://www.ncmem.com/doc/view.aspx?id=a7a1c09aac3b46a5996e32d562d4ae64
            text:{padding:13/*间距*/,background:"#FFA7CODC"/**背景色*/,border:"#FFE64340",round:8/**圆角大小*/,open:false/**打开功能*/}
        }
        //原始图片：http://www.ncmem.com/doc/view.aspx?id=06e3a01a60a84f8e968dd49a5a7c66e8
        , "Original": { name: "original", open: true }
        , Fields: {"uname": "test","upass": "test","uid":"0","fid":"0"}
        , event:{}
        //x86
        , ie: {
              part: { clsid: "9767D337-E10A-4319-8854-E4B0FB635274", name: "Xproer.ScreenCapturePro2" }
            , path: "http://res2.ncmem.com/download/scp2/pack/2.1.12/scp.cab"
        }
        //x64
        , ie64: {
            part: { clsid: "399B59CE-646E-4430-9000-138DF6515306", name: "Xproer.ScreenCapturePro2x64" }
            , path: "http://res2.ncmem.com/download/scp2/pack/2.1.12/scp64.cab"
        }
        , firefox: { name: "", type: "application/npScpPro2", path: "http://res2.ncmem.com/download/scp2/pack/2.1.12/scp.xpi" }
        , chrome: { name: "npScpPro2", type: "application/npScpPro2", path: "http://res2.ncmem.com/download/scp2/pack/2.1.12/scp.crx" }
	    //Chrome 45
        , chrome45: { name: "npScpPro2", path: "http://res2.ncmem.com/download/scp2/pack/2.1.12/scp.crx" }
        , exe: { path: "http://res2.ncmem.com/download/scp2/pack/2.1.12/scp.exe" }
        , edge: {protocol:"zycapture",port:18092,visible:false}
        , "mac": { path: "http://res2.ncmem.com/download/scp2/mac/1.0.36/zyCapture.pkg" }
        , "linux": { path: "http://res2.ncmem.com/download/scp2/linux/1.0.25/com.ncmem.capture_2020.12.3-1_amd64.deb" }
        , "arm64": { path: "http://res2.ncmem.com/download/scp2/arm64/1.0.22/com.ncmem.capture_2020.12.3-1_arm64.deb" }
        , "mips64": { path: "http://res2.ncmem.com/download/scp2/mips64/1.0.19/com.ncmem.wordpaster_2020.12.3-1_mips64el.deb" }
    };

    if (arguments.length > 0) {
        var par = arguments[0];
        if (typeof (par.config) != "undefined") $.extend(true, this.Config, par.config);
        if (typeof (par.event) != "undefined") $.extend(true, this.event, par.event);
        if (typeof (par.ui) != "undefined") $.extend(true, this.ui, par.ui);
    }

    this.loadScripts = function () {
        var head = document.getElementsByTagName('head')[0];
        //加载js
        for (var i = 0, l = this.data.scripts.length;
            i < l;
            ++i) {
            var n = this.data.scripts[i];
            if (-1 != n.lastIndexOf(".css")) {
                _this.api.loadCss(rootDir+n,function(){
                    _this.data.jsCount++;
                    if (_this.data.jsCount == _this.data.scripts.length)
                        _this.event.scriptReady();
                })
            }
            else {
                _this.api.loadJs(rootDir + n, function () {
                    _this.data.jsCount++;
                    if (_this.data.jsCount == _this.data.scripts.length)
                        _this.event.scriptReady();
                });
            }
        }
    };
    
	this.postError = function (json)
	{
        this.OpenMsg();
	    this.ui.msg.html(
            this.data.error[json.value]+"<br/>"+
            "PostUrl:"+this.Config["PostUrl"]+"<br/>"+
            "License2:"+this.Config["License2"]+"<br/>"+
            "当前Url:"+window.location.href);
	    this.ui.per.text("");
	};
	this.postProcess = function (json)
	{
        this.OpenMsg();
	    this.ui.per.text(json.percent);
	};
	this.postComplete = function (json)
	{
	    this.ui.per.text("100%");
	    this.ui.msg.text("上传完成");
	    this.CloseMsg(); //隐藏信息层
        this.event.postComplete(this,json.value);
	};
	this.runComplete = function (json)
	{
	    this.Browser.exitCheck();
	};
    this.loadComplete = function (json) {
        this.data.inited = true;
	    var needUpdate = true;
	    if (typeof (json.version) != "undefined") {
	        if (json.version == this.Config.Version) {
	            needUpdate = false;
	        }
	    }
        if (needUpdate) this.need_update();
        else { this.CloseMsg(); }
	};
    this.load_complete_edge = function (json) {
        this.data.inited = true;
        this.SafeCheck();
        this.CloseMsg();
	    _this.api.init();
	};
    this.afterCapture = function (json) { this.OpenMsg();/*打开信息面板*/ };
    this.hotKey = function (json) { this.event.hotKey(this); }
    this.state_message = function (json) { alert(json.msg); }
	this.recvMessage = function (str)
	{
	    var json = JSON.parse(str);
	    if      (json.name == "AfterCapture") { _this.afterCapture(json); }
        else if (json.name == "HotKey") { _this.hotKey(json); }
        else if (json.name == "state_message") { _this.state_message(json); }
        else if (json.name == "post_process") { _this.postProcess(json); }
	    else if (json.name == "post_error") { _this.postError(json); }
	    else if (json.name == "post_complete") { _this.postComplete(json); }
	    else if (json.name == "run_complete") { _this.runComplete(json); }
	    else if (json.name == "run_error") { _this.postError(json); }
	    else if (json.name == "load_complete") { _this.loadComplete(json); }
	    else if (json.name == "load_complete_edge") { _this.load_complete_edge(json); }
	};

    this.checkBrowser = function () {
        this.data.browser.ie = this.data.browser.name.indexOf("msie") > 0;
        //IE11
        this.data.browser.ie = this.data.browser.ie ? this.data.browser.ie : this.data.browser.name.search(/(msie\s|trident.*rv:)([\w.]+)/) != -1;
        this.data.browser.firefox = this.data.browser.name.indexOf("firefox") > 0;
        this.data.browser.chrome = this.data.browser.name.indexOf("chrome") > 0;    
        this.data.browser.chrome45 = false;
        this.data.browser.edge = this.data.browser.name.indexOf("Edge") > 0;
        this.data.browser.arm64 = this.data.browser.platform.indexOf("aarch64")>0;
        this.data.browser.mips64 = this.data.browser.platform.indexOf("mips64")>0;
        this.chrVer = navigator.appVersion.match(/Chrome\/(\d+)/);
        this.ffVer = this.data.browser.name.match(/Firefox\/(\d+)/);
        if (this.data.browser.edge) { this.data.browser.ie = this.data.browser.firefox = this.data.browser.chrome = this.data.browser.chrome45 = false; }

        //Win64
        if (window.navigator.platform == "Win64")
        {
            $.extend(this.Config.ie,this.Config.ie64);
        }
        else if (this.data.browser.ie) {
    
        }//macOS
        else if (window.navigator.platform == "MacIntel") {
            this.data.browser.edge = true;
            this.api.postMessage = this.api.postMessage2;
            this.api.run = this.api.runChr;
            this.Config.ExePath = this.Config.mac.path;
        }
        else if (window.navigator.platform == "Linux x86_64") {
            this.data.browser.edge = true;
            this.api.postMessage = this.api.postMessageEdge;
            this.api.run = this.api.runChr;
            this.Config.ExePath = this.Config.linux.path;
        }
        else if (this.data.browser.arm64) {
            this.data.browser.edge = true;
            this.api.postMessage = this.api.postMessage2;
            this.api.run = this.api.runChr;
            this.Config.ExePath = this.Config.arm64.path;
        }
        else if (this.data.browser.mips64) {
            this.data.browser.edge = true;
            this.api.postMessage = this.api.postMessage2;
            this.api.run = this.api.runChr;
            this.Config.ExePath = this.Config.mips64.path;
        }//Firefox
        else if (this.data.browser.firefox)
        {
            this.api.postMessage = this.api.postMessage2;
            this.api.run = this.api.runChr;
            this.data.browser.edge = true;
        } //chrome
        else if (this.data.browser.chrome)
        {
            _this.Config["XpiPath"] = _this.Config["CrxPath"];
            _this.Config["XpiType"] = _this.Config["CrxType"];
            
            this.data.browser.edge = true;
            this.api.postMessage = this.api.postMessage2;
            this.api.run = this.api.runChr;
        }
        else if (this.data.browser.edge)
        {
            this.api.postMessage = this.api.postMessage2;
        }
    }

    this.pluginLoad = function () {
        if (!this.data.inited) {
            if (this.data.browser.edge) {
                this.api.connect();
            }
        }
    };
    this.pluginCheck = function () {
        if (!this.data.inited) {
            this.setup_tip();
            this.pluginLoad();
            return false;
        }
        return true;
    };    

	this.GetHtml = function ()
	{
        //ff
        var html = "";
        //ie
	    //html += '<div style="display: none">';
	    html += '<object name="scpIE" classid="clsid:' + this.Config.ie.part.clsid + '"';
	    html += ' codebase="' + this.Config.ie.path + '#version=' + this.Config["Version"] + '" width="1" height="1"></object>';
        if (this.edge) html = '';
	    //html += '</div>';
	    //
        html += '<div name="ui-scp" class="panel-scp">\
	                <img name="ico" alt="进度图标"/><span name="msg">图片上传中...</span><span name="per">10%</span>\
	            </div>';
        //安装提示
        html += '<div name="scp-ui-setup" class="panel-scp panel-setup"><div style="padding:10px;"></div></div>';
	    return html;
	};

    //安全检查，在用户关闭网页时自动停止所有上传任务。
    this.SafeCheck = function (event) {
        //$(window).bind("beforeunload", function (event) {});
        $(window).bind("unload", function () {
            if (this.data.browser.edge) _this.api.close();
        });
    };
	this.setup_tip = function ()
    {       
        this.ui.dialog = layer.open({
            type: 1,
            title: "安装提示",
            closeBtn: 1,
            area: ['291px', '124px'],
            scrollbar: false,
            content: _this.ui.setup,
            end: function () {
                _this.ui.dialog=0;
                _this.ui.setup.hide();
            }
        });
        var dom = this.ui.setup
            .show()
            .find("div")
            .html("<div>控件加载中，如果未加载成功请先</div><span name='setup' class='btn'><img name='setup'/>安装控件</span><span name='setupOk' class='btn'><img name='ok'/>我已安装</span>");
        dom.find('span[name="setup"]').click(function () {
            window.open(_this.Config.exe.path);
        });
        dom.find(".btn").each(function () {
            $(this).hover(function () {
                $(this).addClass("btn-hover");
            }, function () {
                $(this).removeClass("btn-hover");
            });
        });
        dom.find('span[name="setupOk"]').click(function () {
            _this.pluginLoad();
        });
        dom.find("img[name='ok']").attr("src", pathRes + "ok.png");
        dom.find("img[name='setup']").attr("src", pathRes + "setup.png");
    };
    this.need_update = function () {
        var dom = this.ui.setup.find("div").html("发现新版本，请<a name='w-exe' href='#' class='btn'>更新</a>");
        var lnk = dom.find('a[name="w-exe"]');
        lnk.attr("href", this.Config.exe.path);
        this.ui.dialog = layer.open({
            type: 1,
            title: "更新提示",
            closeBtn: 1,
            area: ['170px', '113px'],
            content: _this.ui.setup,
            end: function () {
                _this.ui.dialog=0;
                _this.ui.setup.hide();
            }
        });
    };

    //加载到document.body中
	this.loadAuto= function()
	{
        if(!zyCapture.inited)
        {
            var ui = $(document.body).append(this.GetHtml());
            this.initUI(ui);
        }
        zyCapture.inited = true;
        return this;
    };
	
	//加截到指定对象内部
	this.load_to = function(o)
	{
        if(!zyCapture.inited)
        {
            var ui = o.append(this.GetHtml());
            this.initUI(ui);
        }
        zyCapture.inited = true;
        return this;
    };

    this.initUI = function (ui)
    {
        this.data.parter = ui.find('embed[name="scpFF"]').get(0);
        this.scpIE = ui.find('object[name="scpIE"]').get(0);
        this.ui.panel = ui.find('div[name="ui-scp"]');
        this.ui.ico = ui.find('img[name="ico"]').attr("src", this.Config["IcoPath"]);
        this.ui.msg = ui.find('span[name="msg"]');
        this.ui.per = ui.find('span[name="per"]');
        this.ui.setup = ui.find('div[name="scp-ui-setup"]');

        this.checkBrowser();
        if (!_this.data.browser.edge) {
            if (_this.data.browser.ie) {
                _this.data.parter = _this.scpIE;
            }
            _this.data.parter.recvMessage = _this.recvMessage;
        }
        if (_this.data.browser.edge) {
            _this.api.connect();
        }
        else {
            _this.api.init();
        }
    };
    this.setEditor = function(e) {
        this.data.editor=e;
        return this;
    };

	//截屏函数
    this.Capture = function () {
        if (!this.pluginCheck()) return;
        var opt = { form: this.Config.ShowForm,autoHide: false};//自动隐藏当前窗口
        this.api.capture(opt);
    };
    this.Capture2 = function () {
        if (!this.pluginCheck()) return;
        var opt = { form: this.Config.ShowForm };//不显示提示窗口
        this.api.capture(opt);
    };
    //自动隐藏当前窗口
    this.CaptureHide = function () {        
        if (!this.pluginCheck()) return;
        var opt = { autoHide: true };//自动隐藏当前窗口
        this.api.capture(opt);};

    /**
     * 截取整个屏幕
     * @param bool edit 是否启动编辑
     */
    this.CaptureScreen = function (edit)
    {
        if (!this.pluginCheck()) return;
        this.api.captureScreen({"edit":edit});
    };
	//截取指定区域
    this.CaptureRect = function (x, y, w, h)
    {
        if (!this.pluginCheck()) return;
        this.api.captureRect(x, y, w, h);
    };
    this.OpenMsg = function () {
        
        if(_this.data.opened) return;
        var cfg={width:"350px",height:"160px"};
        if (arguments.length > 0) {
            var par = arguments[0];
            $.extend(true,cfg,par);
        }
        
        _this.ui.dialog = layer.open({
            type: 1,
            title: "提示信息",
            closeBtn: 1,
            area: [cfg.width, cfg.height],
            scrollbar: false,
            content: _this.ui.panel,
            success:function(){
                _this.ui.msg.text("图片上传中...");
            },
            end: function () {
                _this.ui.panel.hide();
                _this.data.opened=false;
            }
        });
        _this.data.opened=true;
    };
    this.Paste = function () {
        if (!this.pluginCheck()) return;
        this.api.paste();
    };
	this.CloseMsg = function(){
        layer.closeAll();
        _this.data.opened=false;
    };

    this.loadScripts();
}
//用法：
/**
 * zyCapture.getInstance().loadAuto();
 */
var zyCapture={
    instance:null,
    inited:false,
    getInstance:function(cfg){
        if (this.instance == null) {
            this.instance = new zyCaptureManager(cfg);
            window.zyCapture = this.instance;
        }
        return this.instance;
    }
};